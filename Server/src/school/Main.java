package school;


import socket.MultiThreadServer;
import utils.JsonExportThread;

import java.io.IOException;

/**
 *  This class is the main application in charge to manage back and JavaFX
 *  @ author N.FOURRIER
 */
public class Main {//NOPMD

    /**
     * The main init the school and export the json file
     * @param args
     *      The arguments given to launch program.
     */
    public static void main(final String[] args) throws IOException  {
        final School school = School.getInstance();
        school.getClasses().forEach(Classe::generateExams);
        // Export to json
        final JsonExportThread export = new JsonExportThread("exportNotes");
        export.start();

        // Starting the socket in a thread
        new MultiThreadServer(8080);
    }
}
