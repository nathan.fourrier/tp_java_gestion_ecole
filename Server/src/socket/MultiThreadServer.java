package socket;

import school.Student;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Logger;

/**
 *  Socket server with multithread to manage
 *  many clients at the same time
 *  @ author N.FOURRIER
 */
public class MultiThreadServer {

    /**
     * Function to start a server multithreaded
     */
    public MultiThreadServer(final int port) throws IOException {
        final ServerSocket server = new ServerSocket(port);
        // Launch server
        System.out.println("Server has started on 127.0.0.1:"+ port+".\r\nWaiting for a connection...");
        while(true){
            final Socket client = server.accept();
            final ClientSocket thread = new ClientSocket(client);
            thread.start();
        }
    }

}
