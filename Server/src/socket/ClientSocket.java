package socket;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import school.School;
import school.Student;

import java.io.*;
import java.net.Socket;
import java.util.logging.Logger;

/**
 *  This class models a client connection and the requests associate to it
 *  it is a thread
 *  @ author N.FOURRIER
 */
public class ClientSocket extends Thread{

    /**
     * Client socket gived in constructor
     */
    private final Socket client;

    /**
     * Constructor
     * @param client socket connection to client
     */
    public ClientSocket(final Socket client) {
        super();
        this.client = client;
    }

    /**
     * Thread run try to call clientRun()
     */
    @Override
    public void run() {
        try {
            this.clientRun();

        } catch (IOException e) {
            e.printStackTrace();//NOPMD
        }
    }

    /**
     * Run client and answer to requests
     */
    private void clientRun() throws IOException {

        try {

            // Generate Json
            final Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            final String json = gson.toJson(School.getInstance());

            final PrintWriter out = new PrintWriter(this.client.getOutputStream(),true);
            final BufferedReader in = new BufferedReader(new InputStreamReader(this.client.getInputStream()));
            System.out.println("A client connected.");
            out.println("READY");

            int state = 0;
            String data ;
            while(state!=2 && (data = in.readLine()) != null){
                switch (state){
                    case 0:
                        //Do smths
                        if(data.contentEquals("GET")) {
                            System.out.println("Client requested a GET");
                            out.println("START");
                            state++;
                        }
                        break;
                    case 1:
                        // Waiting requested content
                        if(data.contentEquals("ALL")){
                            System.out.println("send json");
                            out.println(json);
                            out.println("QUIT");
                            state++;
                        }
                    default:
                        System.out.println(data);
                    break;
                }
            }

        } catch (IOException e) {
            e.printStackTrace();//NOPMD
        }
    }
}
