package enums;

import java.util.Locale;

/**
 *  This enum provide the required subjects
 *  @ author N.FOURRIER
 */
public enum Subject {
    MATHEMATIQUES,
    FRANCAIS,
    ANGLAIS,
    HISTOIRE_GEOGRAPHIE,
    PHYSIQUE,
    SCIENCES_NATURELLES,
    ARTS,
    MUSIQUE,
    SPORT,
    LANGUE_VIVANTE;

    @Override
    public String toString() {
        return this.name().toLowerCase(Locale.FRANCE);
    }
}

