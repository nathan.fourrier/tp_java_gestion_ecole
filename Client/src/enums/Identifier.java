package enums;

/**
 *  This enum provide identifier for each class in a level
 *  @ author N.FOURRIER
 */
public enum Identifier {
    A,
    B,
    C,
    D,
    E,
    F
}
