package enums;

import java.util.Locale;

/**
 *  This enum provide level for a school
 *  @ author N.FOURRIER
 */
public enum Level {
    SIXIEME,
    CINQUIEME,
    QUATRIEME,
    TROISIEME;

    @Override
    public String toString(){
        return this.name().toLowerCase(Locale.FRANCE);
    }
}
