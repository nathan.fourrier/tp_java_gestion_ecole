package enums;

/**
 *  This enum provide optional subjects for students
 *  @ author N.FOURRIER
 */
public enum Option {
    LATIN,
    GREC,
    ANGLAIS_AVANCE
}
