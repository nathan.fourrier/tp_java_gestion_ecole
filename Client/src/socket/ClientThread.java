//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package socket;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import school.School;
import utils.JsonExportThread;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 *  This class is the client connection to server socket
 *  it is threaded
 *  @ author N.FOURRIER
 */
public class ClientThread extends Thread {

    /**
     * ipAdress of the server
     */
    private String ipAddress;

    /**
     * port of the server
     */
    private int port;

    /**
     * state for state machine
     */
    private int state;

    /**
     * Constructor
     * @param ipAddress
     *      Server ip
     * @param port
     *       port number
     */
    public ClientThread(String ipAddress, int port) {
        this.ipAddress = ipAddress;
        this.port = port;
        this.state = 0;
    }

    /**
     * Run the thread
     *
     */
    @Override
    public void run() {
        try {
            this.createClient();
        } catch (IOException var2) {
            var2.printStackTrace();
        }

    }

    /**
     * Connection of client to server
     *
     */
    private void createClient() throws IOException {
        Socket sock = new Socket(this.ipAddress, this.port);
        PrintWriter out = new PrintWriter(sock.getOutputStream());
        BufferedReader in = new BufferedReader(new InputStreamReader(sock.getInputStream()));

        StringBuffer jsonData = new StringBuffer("") ;

        while(this.state != 3) {

            String data = in.readLine();
            switch(this.state) {
                case 0:
                    if (data.compareTo("READY") == 0) {
                        System.out.println("Server Ready");
                        out.println("GET");
                        out.flush();
                        ++this.state;
                    }
                    break;
                case 1:
                    if (data.compareTo("START") == 0) {
                        out.println("ALL");
                        out.flush();
                        System.out.println("Request json");
                        ++this.state;
                    }
                    break;
                case 2:
                    if (data.compareTo("QUIT") == 0) {
                        System.out.println("Server ended the upload");
                        in.close();
                        sock.close();
                        ++this.state;
                    } else {
                        jsonData.append(data);
                    }
                    break;
                default:
                    System.out.println("non" + this.state);
            }
        }
        System.out.println("Transaction ended, json data :");
        System.out.println(jsonData);

        System.out.println("Start import");
        Gson gson = new GsonBuilder().create();
        School ecole = gson.fromJson(jsonData.toString(),School.class);
        School.setInstance(ecole);
        JsonExportThread export = new JsonExportThread("exportNotes");
        export.start();
        System.out.println("imported");

    }
}
