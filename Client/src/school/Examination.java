package school;

import utils.CalculatorUtils;
import com.google.gson.annotations.Expose;
import enums.Subject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

/**
 *  An exam is for the entire class and give
 *  some methods about the stats of the exam
 *  @ author N.FOURRIER
 */
public class Examination {

    /**
     * Hysteresis for variance calculation
     */
    private static final double HYSTERESIS = 2;

    /**
     * Logging from the classe
     */
    private static final Logger LOGGER = Logger.getLogger(Examination.class.getName());

    /**
     * Subject of the exam to identified it
     * Exposed to Gson to export.
     */
    @Expose
    private final Subject subject;

    /**
     * List of marks for each student.
     * Exposed to Gson to export.
     */
    @Expose
    private final List<ExamMark> examMarks;

    /**
     * Constructor
     * @param subject
     *      The subject of the exam.
     */
    public Examination(final Subject subject) {
        this.subject = subject;
        this.examMarks = new ArrayList<>();
    }

    /**
     * Getter
     * @return The subject of the Exam
     */
    public Subject getSubject() {
        return subject;
    }

    /**
     * Getter
     * @return The list of marks for each students.
     */
    public List<ExamMark> getMarks() {
        return examMarks;
    }

    /**
     * Add a Mark of a student.
     * @param student
     *         The student who get this mark
     * @param value
     *          The mark value of the student between 0 and 20
     */
    public void addMark(final Student student, final double value) {
        final ExamMark newExamMark = new ExamMark(this.subject, student, value);
        this.examMarks.add(newExamMark);
        student.getGrades().add(newExamMark);
    }

    /**
     * @return The minimum mark in the list.
     */
    public double getMin() {
        double min = 20;
        for (final ExamMark examMark : this.getMarks()) {
            if (examMark.getMark() < min) {
                min = examMark.getMark();
            }
        }
        return min;
    }

    /**
     * @return The maximum of the list.
     */
    public double getMax() {
        double max = 0;
        for (final ExamMark examMark : this.getMarks()) {
            if (examMark.getMark() > max) {
                max = examMark.getMark();
            }
        }
        return max;
    }

    /**
     * @return The average of the list.
     */
    public double getAvg() {
        double avg = 0;
        for (final ExamMark examMark : this.getMarks()) {
            avg += examMark.getMark();
        }

        return CalculatorUtils.round(avg / this.getMarks().size(), 2);
    }

    /**
     * @return The median of the list
     */
    public double getMedian() {
        double median;
        final ArrayList<Double> sortedMarks = new ArrayList<>();

        for (final ExamMark examMark : this.getMarks()) {
            sortedMarks.add(examMark.getMark());
        }
        Collections.sort(sortedMarks);
        // 20 students --> median is avg of 10th and 11th mark
        median = (sortedMarks.get(10) + sortedMarks.get(11)) / 2.0;

        return CalculatorUtils.round(median, 2);
    }

    /**
     * @return The variance of the list.
     */
    public double getVariance() {
        final double moyenne = this.getAvg();
        double variance = 0;
        for (final ExamMark examMark : getMarks()) {
            variance += Math.pow(examMark.getMark() - moyenne, 2);
        }


        return CalculatorUtils.round(variance, 2);
    }

    /**
     * @return The standard deviation of the list.
     */
    public double getStdDev() {

        return CalculatorUtils.round(Math.sqrt(this.getVariance()), 2);
    }




    /**
     * Method to display with the logger the
     * marks of the students to the Exam
     */
    public void printMarks() {
        LOGGER.fine(this.subject.toString() + ":");//NOPMD
        this.examMarks.forEach((ExamMark examMark) -> {
            LOGGER.fine("    " + examMark.getStudent()  + ": " + examMark.getMark() + "/20");//NOPMD
        });
    }
}
