package school;

import com.google.gson.annotations.Expose;
import enums.Identifier;
import enums.Level;

import java.util.ArrayList;
import java.util.List;

/**
 *  This class represent a school
 *  composed of classes, students and exams
 *  This is a singleton
 *  @ author N.FOURRIER
 */
public final class School {

    /**
     * Instance of the School to made the singleton
     */
    private static School instance;

    /**
     * List of the classes
     * exposed to Gson for export
     */
    @Expose
    private final List<Classe> classes;

    /**
     * Get instance to made singleton
     */
    public static School getInstance() {
        while(School.instance == null){
            try{
                System.out.println("wait");
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


        }
        return instance;
    }

    /**
     * Get instance to made singleton
     */
    public static void setInstance(School school) {

        School.instance = school;
        System.out.println("success");

    }

    /**
     * Private constructor
     * singleton
     */
    private School() {
        this.classes = new ArrayList<>();
        for (final Level level : Level.values()) {
            for (final Identifier identifier : Identifier.values()) {
                final Classe classe = new Classe(level, identifier);//NOPMD
                classes.add(classe);
            }
        }

        initExams();
    }

    /**
     * Getter
     * @return the list of the classes
     */
    public List<Classe> getClasses() {
        return classes;
    }

    /**
     * Create the exams with random marks
     */
    private void initExams(){
        classes.forEach(Classe::generateExams);
    }

    /**
     * Getter
     * List of the classes filter by level
     * @param level the level to filter
     * @return the list filtered
     */
    public List<Classe> getClassesByLevel(final Level level) {
        final List<Classe> levelClasses = new ArrayList<>();
        for (final Classe classe : this.classes) {
            if (classe.getLevel() == level) {
                levelClasses.add(classe);
            }
        }
        return levelClasses;
    }
}
