package school;

import com.github.javafaker.Faker;
import utils.CalculatorUtils;
import com.google.gson.annotations.Expose;
import enums.Option;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 *  A student is a member of a classe
 *  and participate to exams so have marks
 *  @ author N.FOURRIER
 */
public class Student {

    /**
     * Logger to verbose the list of students
     */
    private static final Logger LOGGER = Logger.getLogger(Student.class.getName());

    /**
     * name of the student
     * exposed to Gson to export
     */
    @Expose
    private final String lastName;

    /**
     * first name of the student
     * exposed to Gson to export
     */
    @Expose
    private final String firstName;

    /**
     * HashMap of options of the specific student
     * each option had a note
     */
    private final HashMap<Option,Double> options;

    /**
     * List of the marks of the student
     */
    private final List<ExamMark> grades;

    /**
     * Constructor
     * Auto generate all the datas
     */
    public Student() {
        final Faker faker = new Faker();
        this.lastName = faker.name().lastName();
        this.firstName = faker.name().firstName();

        this.options = new HashMap<>();
        this.grades = new ArrayList<>();

        // Adding or not randomly the advanced english option
        if(CalculatorUtils.randomBoolean()){
            options.put(Option.ANGLAIS_AVANCE, CalculatorUtils.getRandomMark());
        }

        // Adding dead language option
        if(CalculatorUtils.randomBoolean()){
            options.put(CalculatorUtils.randomBoolean() ? Option.LATIN : Option.GREC, CalculatorUtils.getRandomMark());
        }
    }

    /**
     * Getter
     * @return lastname of student
     */
    public String getLastName() {
        return this.lastName;
    }

    /**
     * Getter
     * @return first  name of student
     */
    public String getFirstName() {
        return this.firstName;
    }

    /**
     * Getter
     * @return concatenation of firstName lastName
     */
    public String getFullName() {
        return this.firstName + " " + this.lastName;
    }

    /**
     * @return average of the student (with optionals subjects)
     */
    public double getAvg() {
        double avg=0;

        for(final ExamMark examMark : this.getGrades()) {
            avg += examMark.getMark();
        }
        avg = CalculatorUtils.round(avg / this.getGrades().size(),2);

        // Loop throught the options HashMap
        for(final Map.Entry<Option, Double> entry : this.getOptions().entrySet()) {
            final Option option = entry.getKey();
            final Double note = entry.getValue();

            avg += CalculatorUtils.round(0.1 * (note - 10),2);
        }
        return avg;
    }

    /**
     * Getter
     * @return optionals subjects of the student
     */
    public HashMap<Option,Double> getOptions() {
        return this.options;
    }

    /**
     * Getter
     * @return list of the student marks
     */
    public List<ExamMark> getGrades() {
        return this.grades;
    }

    /**
     * print the grades using logger
     */
    public void printGrades(){

        LOGGER.fine("Grades of " + this.firstName + this.lastName);//NOPMD
        this.grades.forEach(examMark ->  {
            LOGGER.fine(examMark.getSubject() + ": " + examMark.getMark() );//NOPMD
        });
    }

}

