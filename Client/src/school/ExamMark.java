package school;

import com.google.gson.annotations.Expose;
import enums.Subject;

/**
 *  This class models a mark of
 *  a student at an exam
 *  @ author N.FOURRIER
 */
public class ExamMark {

    /**
     * Subject of the mark.
     */
    private final Subject subject;

    /**
     * Student who pass the exam.
     * exposed to Gson for export.
     */
    @Expose
    private final String student;

    /**
     * value of the mark
     * exposed to Gson for export.
     */
    @Expose
    private  final double value;

    /**
     * Constructor
     * @param subject subject of the mark from Subject enum
     * @param student student object who pass the exam.
     * @param value value of the mark
     */
    public ExamMark(final Subject subject, final Student student, final double value) {
        this.subject = subject;
        this.student = student.getFullName();
        this.value = value;
    }

    /**
     * Getter
     * @return student who pass of the exam
     */
    public String getStudent() {
        return student;
    }

    /**
     * Getter
     * @return subject of the exam
     */
    public Subject getSubject() {
        return subject;
    }

    /**
     * Getter
     * @return value of the note
     */
    public double getMark() {
        return value;
    }
}
