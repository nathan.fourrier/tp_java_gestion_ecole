package school;

import com.google.gson.annotations.Expose;
import enums.Identifier;
import enums.Level;
import enums.Subject;
import utils.CalculatorUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is a school classe
 * it contain students (20)
 *
 * @ author N.FOURRIER
 */
public class Classe {

    /**
     * Contain the level of the classe to create
     * its name and check some conditions relative
     * to it.
     */
    private final Level level;

    /**
     * Contain the identifier of the classe to create
     * its name and check some conditions relative
     * to it.
     */
    private final Identifier identifier;

    /**
     * List of students are members of the classe
     */
    private final List<Student> students;

    /**
     * List of subjects studied in class
     * a whole classe study the same subjects
     */
    private transient final List<Subject> subjects;

    /**
     * List of exams withs marks
     * Field exposed to Gson for export.
     */
    @Expose
    private final List<Examination> examinations;

    /**
     * Name of classe only used for
     * Gson to export.
     */
    @Expose
    private final String name;

    /**
     * Constructor of the Classe
     *
     * @param level      The level of classe from enum Level.
     * @param identifier The identifier of classe from enum Identifier.
     */
    public Classe(final Level level, final Identifier identifier) {

        this.level = level;
        this.identifier = identifier;
        this.name = this.level.toString() + " " + this.identifier.toString();
        this.students = new ArrayList<>();
        this.subjects = new ArrayList<>();
        this.examinations = new ArrayList<>();

        //Generating students
        for (int i = 0; i < 20; i++) {
            final Student student = new Student();//NOPMD
            this.students.add(student);
        }

        // Adding subjects
        for (final Subject subject : Subject.values()) {
            // The SIXIEMES dont had LANGUE_VIVANTE or PHYSIQUE
            if ((subject == Subject.LANGUE_VIVANTE || subject == Subject.PHYSIQUE) && this.level == Level.SIXIEME) {
                continue;
            }
            this.subjects.add(subject);
        }

    }

    /**
     * Getter
     *
     * @return level of the classe
     */
    public Level getLevel() {
        return level;
    }

    /**
     * Getter
     *
     * @return Identifier of the classe
     */
    public Identifier getIdentifier() {
        return identifier;
    }

    /**
     * Getter
     *
     * @return name of the classe (String concatenation of level and identifier)
     */
    public String getName() { return name; }

    /**
     * Getter
     *
     * @return the list of Exams realised in this classe
     */
    public List<Examination> getExams() {
        return examinations;
    }

    /**
     * Getter
     *
     * @return the list of students in this class
     */
    public List<Student> getStudents() {
        return students;
    }

    /**
     * Getter
     * @param subject
     *         The subject to filter the marks.
     *
     * @return the calculate avg of all the marks of a subject
     */
    public double getAvg(final Subject subject) {
        final ArrayList<Double> avgs = new ArrayList<>();
        double sum = 0;
        getExams().stream()
                .filter(examination -> examination.getSubject() == subject)
                .forEach(examination -> {
                    avgs.add(examination.getAvg());
                });
        for (final double avg : avgs) {
            sum += avg;
        }
        return CalculatorUtils.round(sum / avgs.size(), 2);

    }

    /**
     * Getter
     *
     * @return the average of all students
     *          general average marks
     */
    public double getAvg() {
        final ArrayList<Double> avgs = new ArrayList<>();
        double sum = 0;
        getStudents().stream()
                .forEach(student -> {
                    avgs.add(student.getAvg());
                });
        for (final double avg : avgs) {
            sum += avg;
        }
        return CalculatorUtils.round(sum / avgs.size(), 2);

    }

    /**
     * Generation of an exam with random note for
     * each student
     *
     * @param subject
     *          The subject of the exam.
     */
    public void generateExam(final Subject subject) {
        final Examination examination = new Examination(subject);
        this.examinations.add(examination);

        this.students.forEach((Student student) -> {
            examination.addMark(student, CalculatorUtils.getRandomMark());
        });
    }

    /**
     * Generate all the exams for each subject
     * studied in the classe.
     */
    public void generateExams() {
        this.subjects.forEach((Subject subject) -> {
            // All exams got 3 marks, except SPORT and MUSIC
            final int marksCounts = subject == Subject.SPORT || subject == Subject.MUSIQUE ? 2 : 3;
            for (int i = 0; i < marksCounts; i++) {
                generateExam(subject);
            }
        });

    }

}
