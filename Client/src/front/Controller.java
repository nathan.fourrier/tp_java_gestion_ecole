package front;

import javafx.fxml.Initializable;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Logger;

/**
 *  This controller is for the main UI of the application
 *  and the tabs are separated in packages
 *  @ author N.FOURRIER
 */
public class Controller implements Initializable {//NOPMD

    /**
     * This logger permit to inform the user of the launching of the app
     */
    private static final Logger LOGGER = Logger.getLogger(Controller.class.getName());
    @Override
    public void initialize(final URL fxmlFileLocation, final ResourceBundle resources) {
        LOGGER.fine("init main");
    }
}
