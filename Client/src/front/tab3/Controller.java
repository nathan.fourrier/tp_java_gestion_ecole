package front.tab3;

import enums.Level;
import enums.Subject;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.ComboBox;
import school.Classe;
import school.School;
import static utils.StreamsUtils.*;
import utils.CalculatorUtils;


import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;




/**
 *  This controller is dedicated to the tab3 of the UI
 *  it permit to display the average between different levels
 *  @ author N.FOURRIER
 */
public class Controller implements Initializable {//NOPMD

    /**
     * Getting school singleton to get datas
     */
    private transient final School school = School.getInstance();

    /**
     * ComboBox to choose the subject
     */
    @FXML
    private transient ComboBox subjectSelector;

    /**
     * Chart to display data
     */
    @FXML
    private transient BarChart<String, Number> barChart;

    /**
     * Initialisation of tab
     */
    @Override
    public void initialize(final URL url, final ResourceBundle resourceBundle) {
        // Tab 3
        barChart.setAnimated(false);
        barChart.getXAxis().setAnimated(false);
        barChart.getYAxis().setAutoRanging(false);

        // Init matières
        for (final Subject subject : Subject.values()) {
            subjectSelector.getItems().add(subject.toString());
        }

    }

    /**
     * Displaying data
     */
    @FXML
    private void displayChart() {//NOPMD

        barChart.getData().clear();


        if (subjectSelector.getValue() != null) {
            final Subject selectedSubject = Subject.valueOf(subjectSelector.getValue().toString().toUpperCase(Locale.FRANCE));

            final XYChart.Series dataSeries = new XYChart.Series();
            dataSeries.setName(selectedSubject.toString());

            barChart.setTitle("Moyennes en " + selectedSubject);


            //Classes concernées

            for (final Level level : Level.values()) {
                final List<Classe> classes = getArrayListFromStream(school.getClasses().stream()
                        .filter((Classe classe) -> {
                            return classe.getLevel() == level;
                        }));

                final List<Double> moyennes = new ArrayList<>();//NOPMD
                double sum = 0;
                classes.stream()
                        .forEach(classe -> {
                            moyennes.add(classe.getAvg(selectedSubject));
                        });
                for (final double moyenne : moyennes) {
                    sum += moyenne;
                }
                final double moyenne = CalculatorUtils.round(sum / classes.size(), 2);
                dataSeries.getData().add(new XYChart.Data(level.toString() + "s", moyenne));//NOPMD


            }
            barChart.getData().add(dataSeries);
        }
    }
}
