package front;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import socket.ClientThread;
import school.School;

/**
 *  This class is the main application in charge to manage back and JavaFX
 *  @ author N.FOURRIER
 */
public class Main extends Application {//NOPMD

    /**
     * The main init the school and export the json file
     * @param args
     *      The arguments given to launch program.
     */
    public static void main(final String[] args) {

        ClientThread client = new ClientThread("127.0.0.1", 8080);
        client.start();
        launch(args);

    }

    @Override
    public void start(final Stage primaryStage) throws Exception {

        final Parent root = FXMLLoader.load(this.getClass().getResource("sample.fxml"));

        primaryStage.setTitle("School Manager");
        primaryStage.setScene(new Scene(root, 1200, 600));
        primaryStage.setResizable(false);

        primaryStage.show();

    }
}
