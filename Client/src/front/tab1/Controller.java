package front.tab1;

import enums.Level;
import enums.Subject;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import school.Classe;
import school.School;

import java.net.URL;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 *  This controller is dedicated to the tab1 of the UI
 *  it permit to compare average by subject
 *  @ author N.FOURRIER
 */
public class Controller implements Initializable {//NOPMD



    /**
     * Getting school singleton to get datas
     */
    private final transient School school = School.getInstance();

    /**
     * ComboBox to choose the class level
     */
    @FXML
    private transient ComboBox<String> levelSelector;

    /**
     * ComboBox to choose the subject
     */
    @FXML
    private transient ComboBox<String> subjectSelector;

    /**
     * CheckBox to decide to replace or add to the chart the new datas
     */
    @FXML
    private transient CheckBox addCheckBox;

    /**
     * BarChart to display all the datas requested
     */
    @FXML
    private transient BarChart<String, Number> chart;

    /**
     * Initialisation of the tab
     */
    @Override
    public void initialize(final URL url, final ResourceBundle resourceBundle) {

        // Chart animation
        chart.setAnimated(false);
        chart.getXAxis().setAnimated(false);//NOPMD
        chart.getYAxis().setAutoRanging(false);//NOPMD


        // Init levels
        for (final Level level : Level.values()) {
            final String levelString = level.toString();
            levelSelector.getItems().add(levelString);//NOPMD
        }
        // Init subjects
        for (final Subject subject : Subject.values()) {
            final String subjectString = subject.toString();
            subjectSelector.getItems().add(subjectString);
        }

    }

    /**
     * Display chart the "add" button is pressed
     * passing the state of checkBox
     */
    @FXML
    private void addBtn() {//NOPMD
        displayChart(addCheckBox.isSelected());
    }

    /**
     * Clear the chart when click the clear button
     */
    @FXML
    private void clearBtn() {//NOPMD//NOPMD
        chart.getData().clear();
    }

    /**
     * Displaying the chart with selected datasS
     */
    private void displayChart(final boolean add) {
        if (!add) {
            chart.getData().clear();//NOPMD
            chart.getData().clear();//NOPMD
        }

        if (subjectSelector.getValue() != null && levelSelector.getValue() != null) {//NOPMD

            // Getting value of choice box
            final String subjectString = subjectSelector.getValue().toString().toUpperCase(Locale.FRANCE);
            final Subject selectedSubject = Subject.valueOf(subjectString);

            // Getting value of choice box
            final String levelString = levelSelector.getValue().toString().toUpperCase(Locale.FRANCE);
            final Level selectedLevel = Level.valueOf(levelString);

            // Creating chart series
            final XYChart.Series dataSeries1 = new XYChart.Series();
            dataSeries1.setName(selectedSubject.toString());

            // Chart title
            chart.setTitle("Moyennes en " + selectedSubject + " des " + selectedLevel + "s");

            // Filter classes
            final ArrayList<Classe> classes = new ArrayList<>();
            school.getClasses().stream()
                    .filter((Classe classe) -> {
                        return classe.getLevel() == selectedLevel;
                    })
                    .forEach(classes::add);

            // Adding examinations marks to series
            classes.forEach((Classe classe) -> {
                dataSeries1.getData().add(new XYChart.Data(classe.getName(), classe.getAvg(selectedSubject)));
            });

            // Add it to the chart
            chart.getData().add(dataSeries1);
        }
    }
}
