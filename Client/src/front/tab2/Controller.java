package front.tab2;

import enums.Level;
import enums.Subject;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.ComboBox;
import school.Classe;
import school.ExamMark;
import school.Examination;
import school.School;

import java.net.URL;
import java.util.*;
import java.util.logging.Logger;

/**
 *  This controller is dedicated to the tab2 of the UI
 *  it permit to display the distribution of marks
 *  @ author N.FOURRIER
 */
public class Controller implements Initializable {//NOPMD

    /**
     * Logger used to debug
     */
    private static final Logger LOGGER = Logger.getLogger(Controller.class.getName());

    /**
     * Getting school singleton to get datas
     */
    private final transient School school = School.getInstance();

    /**
     * ComboBox to choose the class level
     */
    @FXML
    private transient ComboBox levelSelector;

    /**
     * ComboBox to choose the subject
     */
    @FXML
    private transient ComboBox subjectSelector;

    /**
     * ComboBox to choose which exam to display
     */
    @FXML
    private transient ComboBox examSelector;

    /**
     * Chart to draw class A
     */
    @FXML
    private transient BarChart<String, Number> barChartA;

    /**
     * Chart to draw class B
     */
    @FXML
    private transient BarChart<String, Number> barChartB;

    /**
     * Chart to draw class C
     */
    @FXML
    private transient BarChart<String, Number> barChartC;

    /**
     * Chart to draw class D
     */
    @FXML
    private transient BarChart<String, Number> barChartD;

    /**
     * Chart to draw class E
     */
    @FXML
    private transient BarChart<String, Number> barChartE;

    /**
     * Chart to draw class F
     */
    @FXML
    private transient BarChart<String, Number> barChartF;

    /**
     * Initialisation of the tab
     */
    @Override
    public void initialize(final URL url, final ResourceBundle resourceBundle) {

        barChartA.setAnimated(false);
        barChartA.getYAxis().setAutoRanging(false);

        barChartB.setAnimated(false);
        barChartB.getYAxis().setAutoRanging(false);

        barChartC.setAnimated(false);
        barChartC.getYAxis().setAutoRanging(false);

        barChartD.setAnimated(false);
        barChartD.getYAxis().setAutoRanging(false);

        barChartE.setAnimated(false);
        barChartE.getYAxis().setAutoRanging(false);

        barChartF.setAnimated(false);
        barChartF.getYAxis().setAutoRanging(false);

        // Init niveaux
        for (final Level level : Level.values()) {
            levelSelector.getItems().add(level.toString());
        }
        // Init matières
        for (final Subject subject : Subject.values()) {
            subjectSelector.getItems().add(subject.toString());
        }

    }

    /**
     * When subject is selected, count the available exams
     * to propose it to the user
     */
    @FXML
    private void countExams() {//NOPMD

        examSelector.getItems().clear();
        if (subjectSelector.getValue() != null && levelSelector.getValue() != null) {
            final Level selectedLevel = Level.valueOf(levelSelector.getValue().toString().toUpperCase(Locale.FRANCE));
            final Subject selectedSubject = Subject.valueOf(subjectSelector.getValue().toString().toUpperCase(Locale.FRANCE));

            if(!(selectedLevel == Level.SIXIEME && (selectedSubject == Subject.PHYSIQUE || selectedSubject == Subject.LANGUE_VIVANTE))){
                examSelector.getItems().add("1");
                examSelector.getItems().add("2");

                if (selectedSubject != Subject.SPORT && selectedSubject != Subject.MUSIQUE) {
                    examSelector.getItems().add("3");
                }
            }

        }

    }

    /**
     * Display Examination datas to a chart
     * @param barChart the barchat chere to display datas
     */
    void displayExam(BarChart<String, Number> barChart,String title, Examination examination ){
        barChart.getData().clear();

        // Chart title
        barChart.setTitle(title);
        System.out.println(title);
        // Adding examinations marks to series
        for(ExamMark mark : examination.getMarks()){
            final XYChart.Series dataSeries1 = new XYChart.Series();
            dataSeries1.getData().add(new XYChart.Data("",mark.getMark()));
            // Add it to the chart
            barChart.getData().add(dataSeries1);
        }
    }


    /**
     * Display the charts for the requested exam
     */
    @FXML
    private void displayChart() {//NOPMD
        if (subjectSelector.getValue() != null && levelSelector.getValue() != null && examSelector.getValue() != null) {
            // Getting value of choice box
            final String subjectString = subjectSelector.getValue().toString().toUpperCase(Locale.FRANCE);
            final Subject selectedSubject = Subject.valueOf(subjectString);

            // Getting value of choice box
            final String levelString = levelSelector.getValue().toString().toUpperCase(Locale.FRANCE);
            final Level selectedLevel = Level.valueOf(levelString);

            // Getting value of choice box
            final String examString = examSelector.getValue().toString();
            final int examNb = Integer.parseInt(examString);


            final TreeMap<String, Examination> epreuvesNiveau = new TreeMap<>();

            for (final Classe classe : school.getClasses()) {
                if (classe.getLevel() == selectedLevel) {
                    final ArrayList<Examination> epreuvesMatiere = new ArrayList<>();//NOPMD
                    for (final Examination examination : classe.getExams()) {
                        if (examination.getSubject() == selectedSubject) {
                            epreuvesMatiere.add(examination);
                        }
                    }
                    epreuvesNiveau.put(classe.getIdentifier().toString(), epreuvesMatiere.get(examNb - 1));

                }
            }
            epreuvesNiveau.forEach((k, v) -> {
                switch (k){
                    case "A":
                        displayExam(barChartA,k,v);
                        break;
                    case "B":
                        displayExam(barChartB,k,v);
                        break;
                    case "C":
                        displayExam(barChartC,k,v);
                        break;
                    case "D":
                        displayExam(barChartD,k,v);
                        break;
                    case "E":
                        displayExam(barChartE,k,v);
                        break;
                    case "F":
                        displayExam(barChartF,k,v);
                        break;
                    default:
                        System.out.println("error");
                    break;
                }




            });


        }
    }
}
