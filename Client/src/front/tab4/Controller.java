package front.tab4;

import enums.Level;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.ComboBox;
import school.Classe;
import school.School;
import java.net.URL;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 *  This controller is dedicated to the tab4 of the UI
 *  it permit to display the average for a level
 *  @ author N.FOURRIER
 */
public class Controller implements Initializable {//NOPMD

    /**
     * Getting school singleton to get datas
     */
    private transient final School school = School.getInstance();

    /**
     * ComboBox to choose the class level
     */
    @FXML
    private transient ComboBox levelSelector;

    /**
     * Chart to display data
     */
    @FXML
    private transient BarChart<String, Number> barChart;

    /**
     * Initialisation of the tab
     */
    @Override
    public void initialize(final URL url, final ResourceBundle resourceBundle) {
        barChart.setAnimated(false);
        barChart.getXAxis().setAnimated(false);
        barChart.getYAxis().setAutoRanging(false);

        // Init levels
        for (final Level level : Level.values()) {
            levelSelector.getItems().add(level.toString());
        }
    }

    /**
     * Display data to user
     */
    @FXML
    private void displayChart() {//NOPMD

        barChart.getData().clear();

        if (levelSelector.getValue() != null) {

            final Level selectedLevel = Level.valueOf(levelSelector.getValue().toString().toUpperCase(Locale.FRANCE));

            final XYChart.Series dataSeries = new XYChart.Series();
            dataSeries.setName("Moyennes génerales");

            barChart.setTitle("Moyennes en " + " des " + selectedLevel + "s");

            // Filter classes
            final ArrayList<Classe> classes = new ArrayList<>();
            school.getClasses().stream()
                    .filter((Classe classe) -> {
                        return classe.getLevel() == selectedLevel;
                    })
                    .forEach(classe -> classes.add(classe));

            // Examination
            classes.forEach((Classe classe) -> {
                dataSeries.getData().add(new XYChart.Data(classe.getName(), classe.getAvg()));

            });
            barChart.getData().add(dataSeries);
        }
    }
}
