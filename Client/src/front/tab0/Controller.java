package front.tab0;

import enums.Level;
import enums.Subject;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import school.Classe;
import school.School;
import socket.ClientThread;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 *  This controller is dedicated to the tab1 of the UI
 *  it permit to compare average by subject
 *  @ author N.FOURRIER
 */
public class Controller implements Initializable {//NOPMD

    /**
     * Text box to display updating status
     */
    @FXML
    Text textBox;

    /**
     * Initialisation of the tab
     */
    @Override
    public void initialize(final URL url, final ResourceBundle resourceBundle) {

    }

    // TODO: Make the popup to import the txt file
    @FXML
    void browseTxt(){
        FileChooser file = new FileChooser();

    }
    /**
     * Updating school data from server when button pressed
     */
    @FXML
    private void updateDatas(){
        ClientThread client = new ClientThread("127.0.0.1", 8080);
        client.start();
        while(client.isAlive()) {
            System.out.println("Updating...");
            try{
                Thread.sleep(500);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        textBox.setText("Updated");

    }

}
