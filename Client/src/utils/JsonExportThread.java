package utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import school.School;

import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Logger;

/**
 *  This utility class take the school object
 *  and export it to json for exploitation outside of
 *  this program in background with Thread
 *  @ author N.FOURRIER
 */
public final class JsonExportThread extends Thread {

    /**
     * Logger to verbose the export
     */
    private static final Logger LOGGER = Logger.getLogger(JsonExportThread.class.getName());

    /**
     * The output folder to export file
     */
    private static final String OUTPUT_PATH = System.getProperty("user.dir") + "/output/";

    /**
     * The output folder to export file
     */
    private transient final String fileName;

    /**
     * The output folder to export file
     * @param fileName of file to export.
     */
    public JsonExportThread(final String fileName){
        super();
        this.fileName = fileName;
    }

    /**
     * Exporting to json using Gson lib
     *
     */
    @Override
    public void run() {

        final Gson gson = new GsonBuilder().create();
        final String json = gson.toJson(School.getInstance());

        try {
            FileWriter file = new FileWriter(JsonExportThread.OUTPUT_PATH + this.fileName + ".json");//NOPMD
            file.write(json);
            file.close();
        } catch (IOException e) {
            final String errorMessage = e.getCause().toString();
            LOGGER.warning(errorMessage);
        }
        LOGGER.fine("JSON file created: " + json);//NOPMD
    }

}

