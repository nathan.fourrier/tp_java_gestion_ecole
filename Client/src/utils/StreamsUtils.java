package utils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *  This utility class contain methods to use
 *  with stream and permit to make the
 *  code more readable
 *  @ author N.FOURRIER
 */
public final class StreamsUtils {

    /**
     * Private constructor to prevent to instantiate class
     */
    private StreamsUtils(){}

    /**
     * Method to cast / transform a stream and store it to ArrayList
     * @param stream the stream to collect
     * @return the content of the stream into an ArrayList
     */
    public static <T> List<T> getArrayListFromStream(final Stream<T> stream) {
        // Convert the Stream to List
        final List<T> list = stream.collect(Collectors.toList());

        return new ArrayList<T>(list);
    }

}
