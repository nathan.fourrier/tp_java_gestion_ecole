package utils;

/**
 *  This utility class give a list of methods
 *  usefull to calculate simply inside the program
 *  @ author N.FOURRIER
 */
public final class CalculatorUtils {

    /**
     * Private constructor to prevent to instantiate class
     */
    private CalculatorUtils(){}

     /**
     * @return a random boolean true or false
     */
    public static boolean randomBoolean() {
        return Math.random() < 0.5;
    }

    /**
     * @return a random mark between 0 and 20
     */
    public static double getRandomMark() {
        return round(Math.random() * 20, 2);
    }

    /**
     * Round a value with given number of decimals.
     * @param number The number to transform
     * @param decimalNb The number of decimals after decimal point
     * @return list of the student marks
     */
    public static double round(final double number, final int decimalNb) {
        return (double) ((int) (number * Math.pow(10, decimalNb) + .5)) / Math.pow(10, decimalNb);
    }


}
