# Requirement
To build and run the projetct you need

- javafx 15
- jdk15 

# Server
Headless Server (port 8080) to generate data of a school and send it to a client.
To run server run the main class in package School

# Client
Java FX client to monitor data of school server.
To run Client run the main class in package front
